const exampleText = 'Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами';
const arrayWords = exampleText.split(' ');
let countDictionary = 1;

const GetRandomInt = (max) => {
	return Math.floor(Math.random() * Math.floor(max));
}

const GetMassive = () => {
	const lengthArray = arrayWords.length;
	const count = Math.max(3, GetRandomInt(10));
	const startElement = Math.min(lengthArray - 4, GetRandomInt(lengthArray))
	const lastElement = Math.min(lengthArray - 1, startElement + count);

	return arrayWords.slice(startElement, lastElement);
}

const CreateRequest = () => {
	if (Math.random() < 0.1) {
		alert('Произошла ошибка при загрузке, попробуйте снова');
	} else {
		countDictionary++;
		ArrangeBoxContainer(countDictionary, 'Словарь №' + countDictionary, GetMassive());
	}
}

const LoadFile = (input) => {
	const file = input.files[0];
	const reader = new FileReader();

	reader.readAsText(file);

	reader.onload = function () {
		try {
			data = JSON.parse(reader.result);
			if (data.title && data.items && data.items.length > 0) {
				countDictionary++;
				ArrangeBoxContainer(countDictionary, data.title, data.items);
			} else {
				alert("Ошибка данных JSON");
			}
		} catch(e) {
			alert(e);
		}

	};

	reader.onerror = function () {
		console.log(reader.error);
	};
}