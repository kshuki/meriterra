const ListItemContainer = (listItem) => {
	const template = document.getElementById('box-item-template');
	const clone = document.importNode(template.content, true);

	const box = document.createElement('div');
	box.classList.add('list-item-box');

	listItem.forEach((title) => {
		const item = document.importNode(template.content, true);
		item.querySelector('.box-item').textContent = title;
		box.appendChild(item);
	});

	return box;
}


const ArrangeBoxContainer = (id, title, listItem) => {
	const templateBox = document.getElementById('arrange-box-template');
	const arrangeBox = document.importNode(templateBox.content, true);

	const templateBtn = document.getElementById('block-btn-side-template');
	const buttonsLeft = document.importNode(templateBtn.content, true);

	const buttonsRight = document.importNode(templateBtn.content, true);

	const itemsLeft = ListItemContainer(listItem);
	const itemsRight = ListItemContainer([]);
	
	const leftBox = arrangeBox.querySelector('.js-left');
	leftBox.appendChild(buttonsLeft);
	leftBox.appendChild(itemsLeft);

	const rightBox = arrangeBox.querySelector('.js-right');
	rightBox.appendChild(itemsRight);
	rightBox.appendChild(buttonsRight);

	arrangeBox.querySelector('.title').textContent = title;
	arrangeBox.querySelector('.js-container').id = 'arrange-box-' + id;
	document.getElementById('load-btn').before(arrangeBox);
}

ArrangeBoxContainer(1, 'Словарь №1', ['раз', 'два', 'три', 'четыре']);