const listActiveElements = new Map();

const findElement = (elementHTML, classes) => {
	if (elementHTML.classList.contains(classes)) {
		return elementHTML;
	} else {
		return findElement(elementHTML.parentElement, classes);
	}
}

const getKeyDictionary = (elementHTML) => {
	const container = findElement(elementHTML, 'js-container');
	const box = findElement(elementHTML, 'arrange-box');
	const side = box.classList.contains('js-left') ? '-left' : '-right';
	const str = container.id + side;
	return str;
}

const ActiveItemClick = (element) => {
	const activeElements = document.getElementsByClassName('box-item active');

	const key = getKeyDictionary(element);
	const oldElement = listActiveElements.get(key);

	if (listActiveElements.get(key) === void (0)) {
		listActiveElements.set(key, element);
	} else {
		oldElement.classList.remove('active');
		listActiveElements.set(key, element);
	}

	element.classList.add('active');
}


const VerticalMaxMovement = (element, isUpMove = true) => {
	const box = findElement(element, 'arrange-box');
	const selectElement = listActiveElements.get(getKeyDictionary(box));

	if (selectElement !== void (0)) {
		const boxItems = box.querySelector('.list-item-box');

		if (isUpMove) {
			boxItems.prepend(selectElement);
		} else {
			boxItems.append(selectElement);
		}
	}
}

const VerticalMovement = (element, isUpMove = true) => {
	const box = findElement(element, 'arrange-box');
	const selectElement = listActiveElements.get(getKeyDictionary(box));

	if (selectElement === void (0)) return;

	const listItem = box.getElementsByClassName('box-item');
	const isMove = isUpMove ? listItem[0] !== selectElement : listItem[listItem.length - 1] !== selectElement;
	
	const boxSelectElement = findElement(selectElement, 'arrange-box');
	const isLeftSideButton = box.classList.contains('js-left');
	const isLeftSideSelectElement = boxSelectElement.classList.contains('js-left');
	const isOneSide = isLeftSideButton === isLeftSideSelectElement;


	if (isMove && isOneSide) {
		let pos = 0; //позиция элемента в списке относительно
		//которого будем перемещать выбранные (активный элемент)
		for (let i = 0; i < listItem.length; i++) {
			if (selectElement === listItem[i]) {
				pos = isUpMove ? i - 1 : i + 1;
				break;
			}
		}

		if (isUpMove) {
			listItem[pos].before(selectElement);
		} else {
			listItem[pos].after(selectElement);
		}
	}
}


const HorizontalMovementOneElement = (element, isMoveRight = true) => {
	const key = findElement(element, 'js-container').id + (isMoveRight ? '-left' : '-right');
	const selectElement = listActiveElements.get(key);
	if (selectElement === void (0)) return;

	let boxArrange = findElement(selectElement, 'arrange-box');
	const isLeftSide = boxArrange.classList.contains('js-left');

	if (isLeftSide === isMoveRight) {
		const container = findElement(element, 'container-arrange-box');
		const jsClass = isLeftSide ? '.js-right' : '.js-left';
		boxArrange = container.querySelector(jsClass);

		const boxItems = boxArrange.querySelector('.list-item-box');
		boxItems.append(selectElement);
		selectElement.classList.remove('active');
		listActiveElements.delete(key);
	}
}

const HorizontalMovementAll = (element, isMoveRight = true) => {
	const container = findElement(element, 'container-arrange-box');
	const key = container.id +  + (isMoveRight ? '-left' : '-right');
	const leftBoxArrange = container.querySelector('.js-left');
	const rightBoxArrange = container.querySelector('.js-right');

	if (isMoveRight) {
		const listItemLeft = leftBoxArrange.getElementsByClassName('box-item');
		if (listItemLeft.length > 0) {
			const boxItemsRight = rightBoxArrange.querySelector('.list-item-box');
			for (let i = 0; i < listItemLeft.length; i) {
				listItemLeft[i].classList.remove('active');
				boxItemsRight.append(listItemLeft[i]);
			}
		}
	} else {
		const listItemRight = rightBoxArrange.getElementsByClassName('box-item');
		if (listItemRight.length > 0) {
			const boxItemsLeft = leftBoxArrange.querySelector('.list-item-box');
			for (let i = 0; i < listItemRight.length; i) {
				listItemRight[i].classList.remove('active');
				boxItemsLeft.append(listItemRight[i]);
			}
		}
	}
	listActiveElements.delete(key);
}


//перемещение по вертикали
const MoveUpAction = (element) => {
	VerticalMovement(element);
}

const MoveDownAction = (element) => {
	VerticalMovement(element, false);
}

const MoveTopAction = (element) => {
	VerticalMaxMovement(element);
}

const MoveBottomAction = (element) => {
	VerticalMaxMovement(element, false);
}


//перемещение по горизонтали
const MoveRightAction = (element) => {
	HorizontalMovementOneElement(element);
}

const MoveAllRightAction = (element) => {
	HorizontalMovementAll(element, true, true);
}

const MoveLeftAction = (element) => {
	HorizontalMovementOneElement(element, false);
}

const MoveAllLeftAction = (element) => {
	HorizontalMovementAll(element, false, true);
}

const DoubleClick = (element) => {
	const box = findElement(element, 'arrange-box');
	HorizontalMovementOneElement(element, box.classList.contains('js-left'));
}